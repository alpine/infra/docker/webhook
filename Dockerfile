FROM alpine AS base

RUN apk add build-base git go

RUN go get -ldflags "-s -w" github.com/adnanh/webhook

FROM alpine

ENV SSL_CERT_FILE=/etc/ssl/cert.pem

EXPOSE 9000

COPY --from=base /root/go/bin/webhook /usr/local/bin/

USER 1000:1000

ENTRYPOINT [ "webhook" ]
